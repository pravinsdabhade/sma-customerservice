package com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="customer_tab")
public class Customer {

	@Id
	// @GeneratedValue(strategy = GenerationType.SEQUENCE)	// FOR ORACLE
	@GeneratedValue(strategy = GenerationType.IDENTITY) //  FOR MY-SQL
	@Column(name="cust_id_col")
	private Long id;
	
	@Column(name="cust_name")
	private String name;
	
	@Column(name="cust_email")
	private String email;
	
	@Column(name="cust_image")
	private String imagePath;
	
	@Column(name="cust_mobile")
	private String mobile;
	
	@Column(name="cust_addr")
	private String address;
	
	@Column(name="cust_pan")
	private String panCardId;
	
	@Column(name="cust_aadhar")
	private String aadharId;
	
	@Column(name="cust_gender")
	private String gender;
	
}
