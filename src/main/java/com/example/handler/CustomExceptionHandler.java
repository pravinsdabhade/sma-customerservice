package com.example.handler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.example.exception.CustomerNotFoundException;
import com.example.payload.response.ErrorMessage;

@RestControllerAdvice
public class CustomExceptionHandler {

//	If handlerCustomerNotFounException is thrown from any Rest Controller
//	then below method is executed and return internal error message with 500 status code.

//	It is like a reusable catch block code.
	@ExceptionHandler(CustomerNotFoundException.class)
	public ResponseEntity<ErrorMessage> handlerCompanyNotFounException(CustomerNotFoundException cnfe) {
//		return ResponseEntity.internalServerError().body(cnfe.getMessage());
		return ResponseEntity.internalServerError().body(
				new ErrorMessage(
						new Date().toString(), 
						cnfe.getMessage(),
						HttpStatus.INTERNAL_SERVER_ERROR.value(), 
						HttpStatus.INTERNAL_SERVER_ERROR.name()));
	}
}
