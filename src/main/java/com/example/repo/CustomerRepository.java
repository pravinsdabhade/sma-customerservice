package com.example.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

	Optional<Customer> findByEmail(String email);

	Optional<Customer> findByPanCardId(String panCard);

	Optional<Customer> findByAadharId(String aadhar);

	Optional<Customer> findByMobile(String mobile);
}
