package com.example.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.Customer;
import com.example.exception.CustomerNotFoundException;
import com.example.repo.CustomerRepository;
import com.example.service.ICustomerService;

@Service
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	CustomerRepository repo;

	@Override
	public Long saveCustomer(Customer cust) {
		return repo.save(cust).getId();
	}

	@Override
	public Customer getOneCustomer(Long id) {
		Optional<Customer> opt = repo.findById(id);
		return validateInput(opt, id.toString());
	}

	@Override
	public Customer getOneCustomerByEmail(String email) {
		Optional<Customer> opt = repo.findByEmail(email);
		return validateInput(opt, email);
	}

	@Override
	public Customer getOneCustomerByPanCard(String panCard) {
		Optional<Customer> opt = repo.findByPanCardId(panCard);
		return validateInput(opt, panCard);
	}

	@Override
	public Customer getOneCustomerByMobile(String mobile) {
		Optional<Customer> findByMobile = repo.findByMobile(mobile);
		return validateInput(findByMobile, mobile);
	}

	@Override
	public Customer getOneCustomerByAadhar(String aadhar) {
		Optional<Customer> findByAadhar = repo.findByAadharId(aadhar);
		return validateInput(findByAadhar, aadhar);
		
//		if (!findByAadhar.isPresent()) {
//			throw new CustomerNotFoundException("Customer '" + aadhar + "' not found.");
//		} else {
//			return findByAadhar.get();
//		}
	}

	@Override
	public List<Customer> getAllCustomer() {
		return repo.findAll();
	}

	private Customer validateInput(Optional<Customer> opt, String input) {
//		if (!opt.isPresent()) {
//			throw new CustomerNotFoundException("Customer '" + input + "' not found.");
//		} else {
//			return opt.get();
//		}
		
		// Using JDK8
		return opt.orElseThrow(() -> new CustomerNotFoundException("Customer '" + input + "' not found."));
	}

}
