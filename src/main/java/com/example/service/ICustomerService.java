package com.example.service;

import java.util.List;

import com.example.entity.Customer;

public interface ICustomerService {

	Long saveCustomer(Customer cust);

	Customer getOneCustomer(Long id);

	Customer getOneCustomerByEmail(String email);

	Customer getOneCustomerByPanCard(String panCard);

	Customer getOneCustomerByMobile(String mobile);

	Customer getOneCustomerByAadhar(String aadhar);

	List<Customer> getAllCustomer();
}
