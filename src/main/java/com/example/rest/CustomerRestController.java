package com.example.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.Customer;
import com.example.exception.CustomerNotFoundException;
import com.example.service.ICustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerRestController {

	@Autowired
	ICustomerService service;

	// 1. Save Customer
	@PostMapping("/create")
	public ResponseEntity<String> createCustomer(@RequestBody Customer cust) {
		ResponseEntity<String> response = null;
		Long id = service.saveCustomer(cust);
		response = ResponseEntity.ok("Customer '" + id + "' Created!");
		return response;
	}

	// 2. fetch all customer
	@GetMapping("/all")
	public ResponseEntity<List<Customer>> getAllCustomers() {
		ResponseEntity<List<Customer>> res = null;
		List<Customer> allCustomer = service.getAllCustomer();
		res = ResponseEntity.ok(allCustomer);
		return res;
	}

	// 3. fetch one customer
	@GetMapping("/findEmail/{email}")
	public ResponseEntity<Customer> getOneCustomerByEmail(@PathVariable("email") String email) {
		ResponseEntity<Customer> res = null;
		try {
			Customer cust = service.getOneCustomerByEmail(email);
			res = new ResponseEntity<Customer>(cust, HttpStatus.OK);
		} catch (CustomerNotFoundException e) {
			e.printStackTrace();
			throw e;
		}
		return res;
	}
	
	// 4. fetch one customer by pancard
	@RequestMapping(path = "findPanCard/{panCardId}")
	public ResponseEntity<Customer> getOneCustomerByPanCard(@PathVariable("panCardId") String panCard){
		ResponseEntity<Customer> res = null;
		try {
			Customer cust = service.getOneCustomerByPanCard(panCard);
			res = new ResponseEntity<Customer>(cust,HttpStatus.OK);
		}catch(CustomerNotFoundException e) {
			e.printStackTrace();
			throw e;
		}
		return res;
	}
	 
	// 5. fetch one customer by aadharId
	@RequestMapping(path = "findAadhar/{aadharId}")
	public ResponseEntity<Customer> getOneCustomerByAadhar(@PathVariable("aadharId") String aadhar){
		ResponseEntity<Customer> res = null;
		try {
			Customer cust = service.getOneCustomerByAadhar(aadhar);
			res = new ResponseEntity<Customer>(cust, HttpStatus.OK);
		}catch(CustomerNotFoundException e) {
			e.printStackTrace();
			throw e;
		}
		return res;
	}
	
	// 6. fetch one customer by mobile
	@RequestMapping(path = "/findMobile/{mobile}")
	public ResponseEntity<Customer> getOneCustomerByMobile(@PathVariable("mobile") String mobile){
		ResponseEntity<Customer> res = null;
		try {
			Customer cust = service.getOneCustomerByMobile(mobile);
			res = new ResponseEntity<Customer>(cust, HttpStatus.OK);
		}catch(CustomerNotFoundException e) {
			e.printStackTrace();
			throw e;
		}
		return res;
	}
}
